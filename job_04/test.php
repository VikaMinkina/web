<html>

   <head>
      <style>
        .error {color: #FF0000;}
      </style>
   </head>

   <body>
      <?php
				function pvn($var) {
					foreach($GLOBALS as $var_name => $value) {
						if ($value === $var) { return $var_name; }
					}
					return "ВАСЯ";
				}				
				function ec_ho($k){
					echo htmlspecialchars($GLOBALS[pvn($_POST)][$k]);
				}
				function cp($ar){
					echo "<table border-collapse='collapse'><tr><th colspan=2><b>", pvn($ar), "</b></th></tr>\n";
					foreach ($ar as $k => $v) {
						echo "<tr><td>", $k, "=&nbsp</td><td>";
						echo htmlspecialchars($v);
						echo "</td></tr>\n";
					};
					echo "</table><hr>\n";
				}
				//echo "<H3>", $_SERVER["REQUEST_METHOD"], "</H3><hr>";
				//cp($_SERVER);
				//cp($_POST);
				//cp($_COOKIE);
				
				//cp($GLOBALS[pvn($_POST)]);
				$req = $GLOBALS[pvn($_POST)];
				//cp($req);
				//echo "\n\n\n<H1>", $GLOBALS[pvn($_POST)]['name'], "</H1>\n";
				//echo "\n\n\n<H1>", $req['name'], "</H1>\n";
				//echo "\n\n\n<H1>", $req['name'], "</H1>\n";
				//echo "\n\n\n<H1>"; ec_ho('name'); echo "</H1>\n";
				
				//return;
				
        $nameErr = $emailErr = $genderErr = $websiteErr = "";
        $name = $email = $gender = $comment = $website = "";

				if (empty($_POST["name"])) {
					 $nameErr = "Name is required";
				} else {
					$name = test_input($_POST["name"]);
					setcookie('q-name', $name, time()+60);
				}

				if (empty($_POST["email"])) {
					$emailErr = "Email is required";
				}else {
					$email = test_input($_POST["email"]);
					setcookie('q-email', $email, time()+60);
					if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
						$emailErr = "Invalid email format";
					}
				}

				if (empty($_POST["website"])) {
					$website = "";
				}else {
					$website = test_input($_POST["website"]);
					setcookie('q-website', $website, time()+60);
				}

				if (empty($_POST["comment"])) {
					 $comment = "";
				}else {
					 $comment = test_input($_POST["comment"]);
				}

				if (empty($_POST["gender"])) {
					 $genderErr = "Gender is required";
				}else {
					 $gender = test_input($_POST["gender"]);
				}

        function test_input($data) {
					$data = trim($data);
					$data = stripslashes($data);
					$data = htmlspecialchars($data);
					return $data;
        }
      ?>
      <form method = "post" action = "<?php
         echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
         <table>
            <tr>
               <td>Name:</td>
               <td><input type = "text" name = "name"
							  placeholder="Васисуалий Лоханкин"
								value="<?php ec_ho('name');?>" >
                  <span class = "error">* <?php echo $nameErr;?></span>
               </td>
            </tr>
            <tr>
               <td>E-mail: </td>
               <td><input type = "text" name = "email" placeholder="username@server.name"
								value="<?php ec_ho('email');?>" >
                  <span class = "error">* <?php echo $emailErr;?></span>
               </td>
            </tr>
            <tr>
               <td>Time:</td>
               <td> <input type = "text" name = "website">
                  <span class = "error"><?php echo $websiteErr;?></span>
               </td>
            </tr>
            <tr>
               <td>Classes:</td>
               <td> <textarea name = "comment" rows = "5" cols = "40"></textarea></td>
            </tr>
            <tr>
               <td>Gender:</td>
               <td>
                  <input type = "radio" name = "gender" value = "female">Female
                  <input type = "radio" name = "gender" value = "male">Male
                  <span class = "error">* <?php echo $genderErr;?></span>
               </td>
            </tr>
            <td>
               <input type = "submit" name = "submit" value = "Submit">
            </td>
         </table>
      </form>

   </body>
</html>