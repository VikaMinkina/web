<!DOCTYPE html>
<?php
	ini_set('display_errors', '1');
	ini_set('display_startup_errors', '1');
	error_reporting(E_ALL);
	setlocale(LC_ALL, "ru_RU.UTF-8");

	//header('Content-Type: text/html; charset=UTF-8');
	function pvn($var) {
		foreach($GLOBALS as $var_name => $value) {
			if ($value === $var) { return $var_name; }
		}
		return "ВАСЯ";
	}

	$_p = ($_SERVER['REQUEST_METHOD']=="POST");
	$_req_ = $GLOBALS[pvn($_POST)];
	$_saved = FALSE;
	//echo "<H1>", $_SERVER['REQUEST_METHOD'], "name= "; ec_ho('name'); echo "</H1>";
	##cp($_req_);

	function in($fname) {
		global $_req_;
		return in_array($fname,$_req_);
	};
	function ec_ho($k){
		global $_req_, $_p;
		//if (in($k))	{
		if ($_p){
			//if ($_req_[$k]) echo htmlspecialchars($_req_[$k]);
			echo htmlspecialchars($_req_[$k]);
		}
	}
	function cp($ar){
		echo "<table border-collapse='collapse'><tr><th colspan=2><b>", pvn($ar), "</b></th></tr>\n";
		foreach ($ar as $k => $v) {
			echo "<tr><td>", $k, "=&nbsp</td><td>";
			if(is_array($v)){
				var_dump($v);
			}	else {
				echo htmlspecialchars($v);
			}	
			echo "</td></tr>\n";
		};
		echo "</table><hr>\n";
	}
	function pm($p,$s){
		$matches = [];
		$m = preg_match($p, $s ,$matches);
		echo "<pre>\n$p\n$s\n$m\n";
		print_r($matches);
		echo "</pre>";
	}

	$errors = FALSE;
	function ee($fname, $flabel){
		global $errors, $_req_, $_p;
		//if (!in_array($fname,$_req_)) {return "";}
		if (!$_p) return ".";
		if (!empty($_req_[$fname])) return "";
		$errors = TRUE;
		//return "<br>Заполните поле <b>" . $flabel . "</b>";
		return "<br>Заполните поле!";
	}

	$nameErr = $emailErr	= $yearErr = "";
	$sexErr = $limbsErr = $abilityErr = $biographyErr = $introducedErr = "";

	$email= ($_req_['email'] ?? "");
	$name= 	($_req_['name'] ?? "");
	$year= 	($_req_['year'] ?? "");
	$sex = $limbs = $biography = NULL;
	$ability = []; $introduced = FALSE;
	$sexErr = ee('sex','Пол');
	$limbsErr = ee('limbs', 'Количество конечностей');
	$abilityErr = ee('ability', 'Наличие сверхспособностей');
	$biographyErr = ee('biography', 'Биография');
	$introducedErr = ee('introduced', 'Ознакомлен');
	
	//var_dump($sexErr,$limbsErr,$abilityErr,$biographyErr,$introducedErr);
	
	if($sexErr=='')	$sex = $_req_['sex'];
	if($limbsErr=='')	$limbs = $_req_['limbs'];
	if($abilityErr=='')	$ability = $_req_['ability'];
	if($biographyErr=='')	$biography = $_req_['biography'];
	if($introducedErr=='')	$introduced = $_req_['introduced'];

	$p_year = "/^\s*(19|20)\d{2}$/";
	$p_name = "/^[А-Я][а-я]{0,}\s[А-Я][а-я]{1,}(\s[А-ЯA-Z][а-яa-zА-ЯA-Z\-]{1,})?$/u";
	//pm($p_year, $year);
	//pm($p_name, $name);

	if($year)
		if (!preg_match($p_year, $year )){
			$yearErr = "<br><b>Invalid year format</b>";
			$errors = TRUE;
		}
	if($name)
		if (!preg_match($p_name, $name )){
			$nameErr = "<br><b>Invalid name format</b>";
			$errors = TRUE;
		}

	if ($email and !filter_var($email, FILTER_VALIDATE_EMAIL)) {
		$emailErr = "<br><b>Invalid email format</b>";
		$errors = TRUE;
	}

	/*
	var_dump($errors,$name, $year, $email,
		$sex, $limbs, $ability, $biography,
		$introduced);
	*/
	$run = !$errors && $name && $year && $email
		&& $sex && $limbs && $ability && $biography
		&& $introduced;

	if ($run) {
		$ability_immortality = 0; $ability_fly = 0; $ability_through_the_walls =0;
		if (!$ability[0]){
			if ($ability[1]) $ability_immortality = 1;
			if ($ability[2]) $ability_fly = 1;
			if ($ability[3]) $ability_through_the_walls = 1;
		}

		$host = "localhost"; $user = "vika"; $pass = "kolbasik";
		$db = "vika";

		$conn = mysqli_connect($host,$user,$pass, $db);
		if(!$conn){
			die("Connection failed: " . mysqli_connect_error());
		};

		$sql= mysqli_query($conn,
		"CALL `new`('$name', '$email', '$year', '$sex', '$limbs',
		'$ability_immortality', '$ability_fly', '$ability_through_the_walls',
		'$biography')"
		);

		$result = mysqli_query($conn,
		"select *from people where id=(SELECT LAST_INSERT_ID());"
		);
		$num_fields = $result->field_count;
		$fields = $result->fetch_fields();
		$_saved = TRUE;
	}

?>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <style type="text/css">
			body {
				font-size: 100%;
			}
			form label {
				float: left;
				width: 150px;
				margin-bottom: 5px;
				margin-top: 5px;
			}
			label {
				font-weight: bold;
			}
			.box td {
				margin-top: 10px;
				border-radius: 15px;
				border-style: double;
				padding: 4px;
				width: fit-content;
			}
			td {
				valign: baseline;
			}
			.full {
				width: 100%;
				font-weight: bold;
				font-size: 150%;
			}
			input textarea {
				background-color: "#686868"
			}
			.error {
				color: #FF0000;
			}
		</style>
    <title>Аппаратно-программные средства Web.</title>
  </head>
  <body>
    <h1>Минкина Виктория. Web backend.</h1>
		<hr>
    <h2>Задание 04.</h2>
		<h3>Проверка на сервере корректного заполнения обязательных полей формы.</h3>
		<hr>
		<form id="form1" name="form1" method="post" action="index.php">

		<table  border="0" cellpadding="7" cellspacing="7" class="box" >
			<tr><td>
				<label for="name">Фамилия Имя Отчество</label>
				<input type="text" name="name" id="name" size="50"
				placeholder="Васисуа́лий Андреевич Лоха́нкин" required
				value="<?php ec_ho('name'); ?>"
				/>
				<span class="error">*</span>
				<span class = "error"><?php echo $nameErr; ?></span>
			</td><td>
				<label for="email">Email</label>
				<input type="text" name="email" id="email"
				size="30" placeholder="loxankin@yahoo.ru" required
				value="<?php ec_ho('email'); ?>"
				/>
				<span class="error">*</span>
				<span class = "error"><?php echo $emailErr;?></span>

			</td><td>
				<label for="year">Год рождения</label>
				<input type="text" name="year" id="year" size="5"
				placeholder="1999" required
				value="<?php ec_ho('year'); ?>"
				/>
				<span class="error">*</span>
				<span class="error"><?php echo $yearErr;?></span>
			</td></tr>
			<tr><td>
				<label for="sex">Пол</label>
				<input type="radio" name="sex" value="Ж" id="sex_1"
				<?php if($sex=="Ж") echo "checked"?>
				/>Ж
				<input type="radio" name="sex" value="М" id="sex_0"
				<?php if($sex=="М") echo "checked"?>
				/>М
				<span class="error"><?php echo $sexErr;?></span>
			</td><td colspan=2>
			<label for="limbs" >Количество конечностей</label>
				<input type="radio" name="limbs" value="4" id="limbs_4"
				<?php if($limbs=="4") echo "checked"?>
				/>4
				<input type="radio" name="limbs" value="3" id="limbs_3"
				<?php if($limbs=="3") echo "checked"?>
				/>3
				<input type="radio" name="limbs" value="2" id="limbs_2"
				<?php if($limbs=="2") echo "checked"?>
				/>2
				<input type="radio" name="limbs" value="1" id="limbs_1"
				<?php if($limbs=="1") echo "checked"?>
				/>1

				<span class="error"><?php echo $limbsErr;?></span>
			</td></tr>
		<tr><td>
			<div class="box">
			<label for="ability">Наличие сверхспособностей</label>
			<select name="ability[]" id="ability" multiple="multiple" class="box">
				<option value="0"	<?php if(in_array("0",$ability)) echo "selected" ?>
				>Таковые отсутствуют</option>
				<option value="1"	<?php if(in_array("1",$ability)) echo "selected" ?>
				>Обладаете бессмертием</option>
				<option value="2" <?php if(in_array("2",$ability)) echo "selected" ?>
				>Способность к левитации</option>
				<option value="3" <?php if(in_array("3",$ability)) echo "selected" ?>>
				Способность проникать сквозь стены</option>
			</select>
			<span class="error"><?php echo $abilityErr;?></span>
			</div>
		</td><td colspan=2>
			<div class="box">
			<label for="biography">Биография</label>
			<textarea name="biography" id="biography" cols="65" rows="15"
			placeholder="Не состоял
Не участвовал
Не привлекался"><?php ec_ho('biography'); ?></textarea>
			<span class="error"><?php echo $biographyErr;?></span>
			</div>
		</td></tr>
		<tr><td>
			<div class="box">
			<label for="introduced">Ознакомлен</label>
			<input type="checkbox" name="introduced" value="1" id="introduced" />
			<span class="error"><?php echo $introducedErr;?></span>
			</div>
		</td><td colspan=2>
			<input type="submit" class="full" value="Отправить" />
		</td></tr>
		</table>
		</form>
		<?php
			if ($_saved) echo "<hr><H1>Информация записана в базу!</H1>";
		?>
  </body>
</html>
